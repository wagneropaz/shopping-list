'use strict';

angular.module('shoppingListApp')

.controller('ItemCreateController',function($scope, $state, $stateParams, $modal, Item){

    $scope.item = new Item();
    $scope.item.listId = $stateParams.listId;

    $scope.addItem = function()
    {
    	var f = document.getElementById('file').files[0];
        var r = new FileReader();
        
        function save() {
	    	$scope.item.$save(function () {
	            $state.go('viewList', {id: $stateParams.listId});
	        }, function() {
	            $modal.open({templateUrl: "views/modals/error-default.html", controller: "OkModalCtrl"});
	        });
	    }
    	
	    r.onloadend = function(e) {
	    	$scope.item.image = btoa(r.result);
	    	save();
	    }
	    
	    if(f) {
	    	r.readAsBinaryString(f);
	    }
	    else {
	    	save();
	    }
    };
});
