'use strict';

angular.module('shoppingListApp')

.controller('ListCreateController', function($scope, $state, $stateParams, $modal, List) {

    $scope.list = new List();

    $scope.addList = function()
    {
        $scope.list.$save(function (a, response) {
            $state.go('viewList', {id: $scope.list.id});
        }, function() {
            console.log("oi");
            $modal.open({templateUrl: "views/modals/error-default.html", controller: "OkModalCtrl"});
        });
    };
});
