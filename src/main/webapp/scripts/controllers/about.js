'use strict';

/**
 * @ngdoc function
 * @name shoppingListApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the shoppingListApp
 */
angular.module('shoppingListApp')
  .controller('AboutCtrl', function () {
    console.log("about");
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
