'use strict';

/**
 * @ngdoc function
 * @name shoppingListApp.controller:BannersCtrl
 * @description
 * # BannersCtrl
 * Controller of the shoppingListApp
 */
angular.module('shoppingListApp')
  .controller('BannersCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
