'use strict';

angular.module('shoppingListApp')

.controller('ItemEditController',function($scope, $state, $stateParams, $modal, Item)
{
    $scope.item = Item.get({id: $stateParams.id});
    var item = $scope.item;

    $scope.editItem = function()
    {
    	var f = document.getElementById('file').files[0];
        var r = new FileReader();
        
        function update() {
        	item.$update(function ()
	        {
	            $state.go('viewList', {id: $stateParams.listId});
	        }, function()
	        {
	            $modal.open({templateUrl: "views/modals/error-default.html", controller: "OkModalCtrl"});
	        });
	    }
    	
	    r.onloadend = function(e) {
	    	$scope.item.image = btoa(r.result);
	    	update();
	    }
	    
	    if(f) {
	    	r.readAsBinaryString(f);
	    }
	    else {
	    	update();
	    }
    };
});
