'use strict';

angular.module('shoppingListApp')

.controller('ListEditController', function($scope, $state, $stateParams, $modal, List, Item)
{
    $scope.list = List.get({id: $stateParams.id});
    
    var list = $scope.list;

    $scope.editList = function()
    {
        list.$update(function ()
        {
            $state.go('lists');
        }, function()
        {
            $modal.open({templateUrl: "views/modals/error-default.html", controller: "OkModalCtrl"});
        });
    };
    
    function init() {
    	$scope.allItems = [];
    	$scope.total = 0;
    	$scope.loading = true;
    	
    	var items = Item.query(function () {
			var j = 0;
			for(var i = 0; i < items.length; i++) {
				if(items[i].listId == $stateParams.id) {
					$scope.allItems[j] = items[i];
					$scope.total += items[i].value;
					j++;
				}
			}
			
			$scope.loading = false;
			console.log($scope.allItems);
        });
    }
    init();
    
    $scope.delete = function(item)
    {
        console.log(JSON.stringify(item));
        item.$delete(function()
        {
            console.log("ok");
            init();
        });
    };
    
    $scope.mark = function(item) {
    	console.log("mark: " + JSON.stringify(item));
    	
    	item.$update(function ()
        {
    		//does nothing
        }, function()
        {
            $modal.open({templateUrl: "views/modals/error-default.html", controller: "OkModalCtrl"});
        });
    }
});
