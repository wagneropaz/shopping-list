'use strict';

/**
 * @ngdoc function
 * @name shoppingListApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the shoppingListApp
 */
angular.module('MetaPrioriUtil')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
