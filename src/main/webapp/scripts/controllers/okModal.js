'use strict';

angular.module('shoppingListApp')

.controller('OkModalCtrl', function ($scope, $modalInstance)
{
    $scope.ok = function () {
        $modalInstance.dismiss('ok');
    };
});
