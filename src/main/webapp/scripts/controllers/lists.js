'use strict';

/**
 * @ngdoc function
 * @name shoppingListApp.controller:ListsCtrl
 * @description
 * # ListsCtrl
 * Controller of the shoppingListApp
 */
angular.module('shoppingListApp')

.controller('ListsCtrl', function ($scope, List)
{
    function init() {
        $scope.allLists = [];
        
        $scope.loading = true;
        var lists = List.query(function ()
        {
            $scope.allLists = lists;
            $scope.loading = false;
        });
    }

    init();

    $scope.delete = function(list)
    {
        console.log(JSON.stringify(list));
        list.$delete(function()
        {
            console.log("ok");
            init();
        });
    };
});
