'use strict';

/**
 * @ngdoc service
 * @name shoppingListApp.lists
 * @description
 * # lists
 * Service in the shoppingListApp.
 */
angular.module('dob.services')
  .factory('List', function ($resource, API) {
    return $resource(API.build("/shopping-list/list/:id"), { id: '@id' }, {
      update: {
        method: 'PUT'
      },
      TypeGetCategoryAndBrand: {method: 'get', isArray: true}
    });
  });
