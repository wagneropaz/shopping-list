'use strict';

/**
 * @ngdoc service
 * @name shoppingListApp.configs
 * @description
 * # configs
 * Service in the shoppingListApp.
 */
angular.module('dob.services')
  .factory('API', function () {
    return {
        protocol: "http",
        //host: "localhost:8080/shoppinglist/webresources",
        host: "wagnerpaz.jelasticlw.com.br/webresources",
        build: function (path) {
            return this.protocol + "://" + this.host + path;
        }
    };
  });
