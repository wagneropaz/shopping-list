'use strict';

/**
 * @ngdoc service
 * @name shoppingListApp.items
 * @description
 * # items
 * Service in the shoppingListApp.
 */
angular.module('dob.services')
  .factory('Item', function ($resource, API) {
    return $resource(API.build("/shopping-list/item/:id"), { id: '@id' }, {
      update: {
        method: 'PUT'
      }
    });
  });
