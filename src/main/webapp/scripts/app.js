'use strict';

/**
 * @ngdoc overview
 * @name shoppingListApp
 * @description
 * # shoppingListApp
 *
 * Main module of the application.
 */
angular
    .module('shoppingListApp', [
        'ngAnimate',
        'ngCookies',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ngTouch',
        'ui.router',
        'ui.bootstrap',
        'dob.services',
    ])
    .config(function ($stateProvider) {
        $stateProvider
            .state('home', {
                url: '/',
                templateUrl: 'views/main.html',
                controller: 'MainCtrl',
                controllerAs: 'main'
            })
            .state('about', {
                url: '/about',
                templateUrl: 'views/about.html',
                controller: 'AboutCtrl',
                controllerAs: 'about'
            })
            .state('lists', {
                url: '/lists',
                templateUrl: 'views/lists.html',
                controller: 'ListsCtrl',
                controllerAs: 'lists'
            })
            .state('users', {
                url: '/users',
                templateUrl: 'views/users.html',
                controller: 'UsersCtrl',
                controllerAs: 'users'
            })
            .state('questions', {
                url: '/questions',
                templateUrl: 'views/questions.html',
                controller: 'QuestionsCtrl',
                controllerAs: 'questions'
            })
            .state('banners', {
                url: '/banners',
                templateUrl: 'views/banners.html',
                controller: 'BannersCtrl',
                controllerAs: 'banners'
            })
            .state('addList', {
                url: '/list/new',
                templateUrl: 'views/list-add.html',
                controller: 'ListCreateController'
            })
            .state('viewList',{
                url:'/list/:id/view',
                templateUrl:'views/list-edit.html',
                controller:'ListEditController'
            })
            .state('addItem', {
                url: '/item/new/:listId',
                templateUrl: 'views/item-add.html',
                controller: 'ItemCreateController'
            })
            .state('editItem',{
                url:'/item/:id/edit/:listId',
                templateUrl:'views/item-edit.html',
                controller:'ItemEditController'
            });
    });

/**
 * Other modules (non-main) declaration
 */
angular.module('dob.services', []);
