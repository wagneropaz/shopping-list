package com.wagnerpaz.shoppinglist.resource;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import com.wagnerpaz.shoppinglist.entity.ShoppingItem;
import com.wagnerpaz.shoppinglist.util.PersistenceUtil;

/**
 * Created on 22/09/2015.
 * 
 * @author Wagner Paz
 */
@Path("/shopping-list/item")
public class ShoppingItemResource {

	private EntityManager em = PersistenceUtil.createEm();

	@GET
	public List<ShoppingItem> getAll() {
		return em.createQuery("SELECT l FROM ShoppingItem l", ShoppingItem.class).getResultList();
	}
	
	@Path("/{id}")
	@GET
	public ShoppingItem getById(@PathParam("id") Long id) {
		TypedQuery<ShoppingItem> query = em.createQuery("SELECT l FROM ShoppingItem l WHERE l.id = :id", ShoppingItem.class);
		query.setParameter("id", id);
		return query.getResultList().get(0);
	}
	
	@POST
    public ShoppingItem create(ShoppingItem list) {
		em.getTransaction().begin();
		em.persist(list);
		em.getTransaction().commit();
		
		return list;
    }
	
	@Path("/{id}")
	@PUT
    public void update(@PathParam("id") Long id, ShoppingItem list) {
		em.getTransaction().begin();
		em.merge(list);
		em.getTransaction().commit();
    }
	
	@Path("/{id}")
	@DELETE
    public void remove(@PathParam("id") Long id) {
		em.getTransaction().begin();
		
		ShoppingItem shoppingList = em.find(ShoppingItem.class, id);
		if(shoppingList != null) {
			em.remove(shoppingList);
		}
		
		em.getTransaction().commit();
    }

}
