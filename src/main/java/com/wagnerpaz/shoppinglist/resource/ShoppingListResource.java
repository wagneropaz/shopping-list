package com.wagnerpaz.shoppinglist.resource;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.wagnerpaz.shoppinglist.entity.ShoppingList;
import com.wagnerpaz.shoppinglist.util.PersistenceUtil;

/**
 * Created on 22/09/2015.
 * 
 * @author Wagner Paz
 */
@Path("/shopping-list/list")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ShoppingListResource {
	
	private EntityManager em = PersistenceUtil.createEm();

	@GET
	public List<ShoppingList> getAll() {
		return em.createQuery("SELECT l FROM ShoppingList l", ShoppingList.class).getResultList();
	}
	
	@POST
    public ShoppingList create(ShoppingList list) {
		em.getTransaction().begin();
		em.persist(list);
		em.getTransaction().commit();
		
		return list;
    }
	
	@Path("/{id}")
	@PUT
    public void update(@PathParam("id") Long id, ShoppingList list) {
		em.getTransaction().begin();
		em.merge(list);
		em.getTransaction().commit();
    }
	
	@Path("/{id}")
	@GET
	public ShoppingList getById(@PathParam("id") Long id) {
		TypedQuery<ShoppingList> query = em.createQuery("SELECT l FROM ShoppingList l WHERE l.id = :id", ShoppingList.class);
		query.setParameter("id", id);
		return query.getResultList().get(0);
	}
	
	@Path("/{id}")
	@DELETE
    public void remove(@PathParam("id") Long id) {
		em.getTransaction().begin();
		
		ShoppingList shoppingList = em.find(ShoppingList.class, id);
		if(shoppingList != null) {
			em.remove(shoppingList);
		}
		
		em.getTransaction().commit();
    }

}
