package com.wagnerpaz.shoppinglist.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Created on 23/09/2015.
 * 
 * @author Wagner Paz
 */
public class PersistenceUtil {
	
	private static EntityManagerFactory emf;
	
	static {
		emf = Persistence.createEntityManagerFactory("shoppinglist");
	}
	
	public static EntityManager createEm() {
		return emf.createEntityManager();
	}
}
